package huffman_encoding;

import java.util.Map;

/**
 * Constructs a Huffman Node and implements comparable logic.
 *
 */
public class Node implements Comparable<Node>
{
    char ch;
    int weight;
    Node left;
    Node right;

    Node(){}

    /**
     * Constructs a new Huffman Node from a key-value pair.
     *
     * @param  kvp key-value pair
     *
     */
    Node(Map.Entry kvp)
    {
        this.ch = (char) kvp.getKey();
        this.weight = (int) kvp.getValue();
        this.left = null;
        this.right = null;
    }

    @Override
    public int compareTo(Node o)
    {
        return weight - o.weight;
    }
}
