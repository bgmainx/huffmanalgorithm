package huffman_encoding;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class HuffmanImpl
{
    private static Map<Character, String> prefixCodesMap = new HashMap<>();
    static Node root;
    static int height = 0;
    static int maxHeight = Integer.MIN_VALUE;

    public static void main(String[] args)
    {
        String str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
                " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." +
                " Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." +
                " Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." +
                " Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        PriorityQueue<Node> queue = new PriorityQueue<>();
        HashMap<Character, Integer> charsMap = getFrequencyOfChars(str);

        root = constructHuffmanTree(queue, charsMap);
        generateCodeMap(root, new StringBuilder());

        String encoded = getHuffmanCode(str);

        System.out.printf("Encoded: %s, %nLength: %d%n", encoded, encoded.length());
        System.out.println("Height: " + maxHeight);
    }


    /**
     *
     * @param queue queue to be populated with the constructed Huffman nodes, containing letter and their weight
     * @param charsMap containing each letter and it's frequency
     * @return the root of the Huffman Tree
     */
    private static Node constructHuffmanTree(PriorityQueue<Node> queue, HashMap<Character, Integer> charsMap)
    {
        // fill in the queue with the chars and their frequency
        for (Map.Entry<Character, Integer> entry : charsMap.entrySet())
        {
            Node node = new Node(entry);
            queue.add(node);
        }

        // create the tree and set the root
        while (queue.size() > 1)
        {
            Node node = new Node();
            node.left = queue.poll();
            node.right = queue.poll();
            node.weight = node.left.weight + node.right.weight;
            queue.add(node);
        }

        return queue.poll();
    }

    /**
     * Generates the huffman code.
     *
     * @param str
     */
    private static String getHuffmanCode(String str)
    {
        StringBuilder code = new StringBuilder();

        for (char ch : str.toCharArray())
        {
            code.append(prefixCodesMap.get(ch));
        }

        return code.toString();
    }


    /**
     * Traverses the tree and generates the prefixes for each char. Populates a hash table with them.;
     *
     * Calculates the tree height (i.e. the number of edges between the tree's root and its furthest leaf)
     * in order to avoid another recursive method.
     * @param root the root of the Huffman Tree.
     * @param sb StringBuilder
     */
    private static void generateCodeMap(Node root, StringBuilder sb)
    {
        if (root != null)
        {

            // if null, add the char and it's relevant prefix code
            if (root.left == null && root.right == null)
            {
                prefixCodesMap.put(root.ch, sb.toString());

                if (height > maxHeight)
                {
                    maxHeight = height;
                }
            } else
            {
                height++;
                sb.append("0");
                generateCodeMap(root.left, sb);

                // go one node upper and remove the zero
                sb.deleteCharAt(sb.length() - 1);
                sb.append("1");

                generateCodeMap(root.right, sb);
                sb.deleteCharAt(sb.length() - 1);

                height--;
            }
        }
    }


    /**
     * Calculates frequency оf chars in a string.
     * @param str the string
     * @return hash map with chars in a given string and their frequency.
     */
    private static HashMap<Character, Integer> getFrequencyOfChars(String str)
    {
        HashMap<Character, Integer> temp = new HashMap<>();

        char letter;
        for (int i = 0; i < str.length(); i++)
        {
            letter = str.charAt(i);
            if (!temp.containsKey(letter))
            {
                temp.put(letter, 0);
            }

            temp.put(letter, temp.get(letter) + 1);

        }

        return temp;
    }
}
